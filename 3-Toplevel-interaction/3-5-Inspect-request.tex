\subsection{The inspect request} \label{sec:inspect-request}
\textit{Identified by: "inspect"}

As an additional extra not necessarily part of eFLINT itself, the Specification offers a way of inspecting/downloading (part of) a reasoner's state to a client. This is very useful in the case where part of the reasoner is stateful, but part of it isn't, and the client wants to build a full overview of the reasoner's state. Additionally, this can also be used to retrieve more information in the case of violations or errors.

The inspect request may be used to inspect the entire state or some target properties about defined types or facts. To do so, it adds the following fields to the toplevel request:
\begin{itemize}
    \item {
        \texttt{targets}: An \textit{optional} JSON Array of JSON Strings that may be used to scope the inspect request. Every string is the identifier of some object to retrieve the definition for. Note that only definitions may be requested this way; to inspect the state of specific facts, simply ask boolean queries about their truth value.\\
        If the field is an empty array, it will be interpreted as matching ALL definitions and instances instead of none. If you don't want to request any value, then simply don't send the request.\\
        If omitted, then an empty JSON Array may be assumed.
    }
\end{itemize}

In response to an inspect request, the server sends back an inspect response with the following field:
\begin{itemize}
    \item {
        \texttt{phrases}: A JSON Array of zero or more \textbf{phrase}-types (see section \ref{sec:phrase-type}). This list of phrases together make up the state of the reasoner in such a way that when executed in the specified order, one ends up in the state that the reasoner is in. Ideally, the server should not send back more phrases than necessary.
    }
\end{itemize}

Note that the state of a reasoner \textit{may} actually be private. This means that the server may actually decide to omit specific definitions or instances; in the worst case, the state is completely private, and the server always returns an empty response.

While this property is nice in terms of privacy, it means that one cannot fully rely on an inspect request to fully return the reasoner's state \textit{unless} the client knows a-priori that the server has a fully public state.

An example of an inspect request followed by an inspect response is:
\lstinputlisting[language=json]{Examples/3-Toplevel-interaction/3-5-Inspect-request.json}
