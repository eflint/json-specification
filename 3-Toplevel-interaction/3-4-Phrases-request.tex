\subsection{The phrases request} \label{sec:phrases-request}
\textit{Identified by: "phrases"}

eFLINT is designed to be used from a \textit{Read Eval Print-Loop} (REPL), which means that all of its constructs are represented as so-called \textit{phrases} (see section \ref{sec:phrase-type}). To this end, the JSON Specification defines its interactions with a reasoner in terms of phrases as well. The request to do so is called the \textit{phrases request}.

This request may be used to send zero or more queries, statements, or definitions (i.e., \textit{phrases}) to the eFLINT server. Every phrase is then executed in-order, where results of previous phrases may already influence the result of upcoming phrases within the same request.

The JSON Schema for the phrases request is quite simple:
\begin{itemize}
    \item {
        \texttt{phrases}: A JSON Array with zero or more \textbf{phrase-group}-types (see section \ref{sec:phrase-group-type}). Note that the order in which the groups are specified is is the order in which they are executed; thus, any group inflicting state-changes may influence the result of the next phrase. If this is undesired, consider moving phrases into the same \textbf{phrase-group}.\\
        If any of the groups fail, the server will immediately stop execution. Put differently: it is guaranteed that groups that follow a failed group are not executed.
    }
    \item {
        \texttt{updates}: An \textit{optional} JSON boolean that tells the server that the client is open to receiving state updates. For further information, check the \texttt{updates}-field in the \textbf{phrase}-type (section \ref{sec:phrase-type}).\\
        If omitted, then it may be assumed to be equal to 'false'.\\
        Note that the per-phrase counterpart of this field is leading. If it is given, then the server is expected to always use its value instead of that of the toplevel \texttt{updates}-field.\\
        Also note that, like its per-phrase counterpart, the global \texttt{updates}-field \textit{may} be ignored by the server if it doesn't want to share its state changes.
    }
\end{itemize}

Once such a request is received by the server, it will execute the phrases in the order it received them. For every phrase, it collects its result and sends them back in a single \textit{phrases response}:
\begin{itemize}
    \item {
        \texttt{results}: A JSON Array of zero or more \textbf{phrase-result} definitions (see section \ref{sec:phrase-result-type}). This array is guaranteed to be \textit{at most} as long as the \texttt{phrases} field in the request. Every element then defines a result to the phrase at the same index in the \texttt{phrases} field. If any of the phrases fails, however, this array might be shorter. In that case, its last element will be the result of the phrase that failed, and any subsequent phrases are omitted.\\
        Note that a phrase may fail even if the request itself was well-formed; this may happen if there was something in the state of the reasoner that prevented the phrase from succeeding. As such, the value of the toplevel \texttt{success}-field does not say anything about whether an individual phrase succeeds or not.
    }
\end{itemize}

Given both of these Schemas, the following JSON snippet shows an example interaction:
\lstinputlisting[language=json]{Examples/3-Toplevel-interaction/3-4-Phrases-request.json}

Check the "types"-section (\ref{sec:"types"}) to find the Schemas for the toplevel fields.
