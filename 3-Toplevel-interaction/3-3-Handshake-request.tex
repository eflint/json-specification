\subsection{The handshake request} \label{sec:handshake-request}
\textit{Identified by: "handshake"}

Although the interaction between a client and a server are defined to be entirely stateless (i.e., every request has only one response), it might be useful to perform a kind of 'handshake' beforehand so the client may learn some properties about the server. This may contain, for example, what kind of reasoner the server is running, which version of that reasoner it is running or whether it permits sending violations to the client.

Note, however, that we assume that reasoners may be extremely conservative in what they share. Thus, there are few guarantees about what the server \textit{must} return, meaning that most fields in this response will be optional.

Because this is mostly for the client to get information, the request itself does not add any additional fields. Instead, the server returns a handshake response with the following fields:
\begin{itemize}
    \item {
        \texttt{supported-versions}: A JSON Array with JSON Strings that indicate which versions of the specification are supported by the server. Every string in this Array must be a specification version number as defined in section \ref{sec:about-versioning}.\\
        This field \textit{must} be given by the server, and also \textit{must} at least include the version number that the client sent in its handshake request. If the server does not support it, it should send an error back instead. Thus, if the client sees its requested version omitted, it should assume that the server is not properly working or runs a wildly incompatible version of the specification.
    }
    \item {
        \texttt{supported-extensions}: An \textit{optional} JSON Object that maps specification version numbers to JSON Objects. These nested objects map extension names (as JSON Strings) to JSON Arrays with JSON Strings that list all supported versions of that specific extension. Note, thus, that extension support is namespaced to particular specification versions.\\
        It omitted, then the server does not want to share this information. Similarly, there is nothing preventing the server from providing an incomplete set of supported extensions. Note, however, that it is expected that any listed extension is actually supported.\\
        See the Appendix (section \ref{sec:appendix-proposed-extensions}) for suggested extensions.
    }
    \item {
        \texttt{reasoner}: An \textit{optional} JSON String with some identifier that (uniquely) identifies the reasoner type used.\\
        If omitted, then the server does not want to share this information. Similarly, there is nothing preventing the server from providing an incorrect identifier, so the client should not rely too much on the information shared here.
    }
    \item {
        \texttt{reasoner-version}: An \textit{optional} JSON String that describes the version of the reasoner. The server should only specify this if it is also willing to specify the \texttt{reasoner}-field. How this value should be parsed is reasoner-dependent.\\
        If omitted, then the server does not want to share this information. Similarly, there is nothing preventing the server from providing an incorrect reasoner version, so the client should not rely on the information shared here.
    }
    \item {
        \texttt{shares-updates}: An \textit{optional} JSON boolean that describes whether the server grants requests to share state-changes after state-changing phrases. Note that this is an indication; the server may still decide to refuse this on a per-phrase basis. The function of this field is mainly here to allow reasoners to choose servers that advertise themselves as more cooperative.\\\\
        If omitted, then the server does not want to share this information, and the client can only discover this by trial-and-error.
    }
    \item {
        \texttt{shares-triggers}: An \textit{optional} JSON boolean that describes whether the server grants requests to share details about triggered Acts or Events after state-changing phrases. Note that this is an indication; the server may still decide to refuse this on a per-phrase basis. The function of this field is mainly here to allow reasoners to choose servers that advertise themselves as more cooperative.\\
        If omitted, then the server does not want to share this information, and the client can only discover this by observing if the server shares triggers.
    }
    \item {
        \texttt{shares-violations}: An \textit{optional} JSON boolean that describes whether the server grants requests to share violations. Note that this is an indication; the server may still decide to refuse this on a per-phrase basis. The function of this field is mainly here to allow reasoners to choose servers that advertise themselves as more cooperative.\\
        If omitted, then the server does not want to share this information, and the client can only discover this by observing if the server shares triggers.
    }
\end{itemize}

The following JSON snippets are an example of a valid handshake request and response pair:
\lstinputlisting[language=json]{Examples/3-Toplevel-interaction/3-3-Handshake-request.json}

Because there are no fields that can cause incorrect semantics in the request, it can generally be expected that any response given to a handshake-request is one that is successful (i.e., \texttt{success} is true).
