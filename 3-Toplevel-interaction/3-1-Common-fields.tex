\subsection{Common fields} \label{sec:common-fields}
Across all different request kinds, there are at least two fields that always occurs in every request:
\begin{itemize}
    \item {
        \texttt{version}: A JSON String that contains the version identifier of the specification version used for the remainder of the interaction. This includes both the client and the server; if the latter cannot enforce the specific version or variation, it should return an error immediately.\\
        Note that the specific version can also change the toplevel fields present, and should thus always be inspected first by the server. For example, one can imagine a specification version that has a \texttt{kind}-field and one that doesn't.
    }
    \item {
        \texttt{extensions}: An \textit{optional} JSON Object that maps extension names to optional version numbers. Specifically, the keys in the object are JSON Strings, and the values are either another JSON String with a version number as defined in section \ref{sec:about-versioning}, or null to indicate the latest version should be used. Extensions should only be used if their identifiers occur in this dictionary.\\
        If this field is omitted, it resolves to an empty object.\\
        See the Appendix (section \ref{sec:appendix-proposed-extensions}) for suggested extensions.
    }
    \item {
        \texttt{kind}: A JSON String identifier for the kind of this request. This must equal any of the identifiers specified in the upcoming sections, and is used by the receiving side to determine what other fields to expect in this schema. This should be the second field inspected by the server.
    }
\end{itemize}

For every request kind, there is a response belonging to the same kind. Here, too, there is a number of fields occurring in every response:
\begin{itemize}
    \item {
        \texttt{success}: A JSON boolean value that indicates if the request was received and parsed successfully (true) or not (false). Note that this doesn't actually say anything about the result of the request; instead, it will be set to false if the given request was invalid according to the specification (e.g., unknown version or missing fields). Additionally, it may also return false if the server encountered some unexpected internal error that caused the request to fail. It can be thought of as representing "specification errors", i.e., whether the request could be parsed or processed at all.
    }
    \item {
        \texttt{errors}: An \textit{optional} JSON Array of zero or more \textbf{error} definitions (see subsection \ref{sec:error-type}). This Array is only given \textit{and} populated if \texttt{success} is false, and this field lists what went wrong. Every \textbf{error} definition can be expected to match one reason for failure. If omitted, then it defaults to an empty array.
    }
\end{itemize}

Note that, unlike the requests, the response does not have \texttt{version}-, \texttt{extensions}-, or a \texttt{kind}-fields; this is because the answer expected by the client is always defined by the specification, and does not need further explanation. If necessary, one may still send such fields, which the specification dictates is ignored by any non-interested parties.

Also note that, if \texttt{success} is false, it is very likely that the information in any other field of any of the response-variants is unavailable (because the request failed). Thus, the server may omit any of the variant fields from the response if it failed to process the request.

In the following subsections, we will look at the different request kinds.
