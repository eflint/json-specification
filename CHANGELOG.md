# Changelog for the JSON Specification
This document serves to keep track of changes between eFLINT JSON Specification versions.

Note that the project uses [semantic versioning](https://semver.org). See the 'About versioning' section in the document for more information.

Breaking changes are indicated by "\[**breaking change**\]". Note that any updates to extensions have no effect on the semantic version of the API. As such, breaking extension changes are reprsented as "(breaking change)".


## v3.0.0 - 2024-08-30
This update makes the specification compatible with eFLINT 4. It also embraces its eFLINT-specificness by adopting the operators into the specification itself, and adds the missing `Var`- and `Function`-types.

### Added
- The new `phrase-group`-statements.
- `Var`-facts as modifiers to atomic/composite facts (`is-var`-field).
- `Function`-facts as new phrase variants (`ffact`).
- `identified-by` is now marked as extendable with `extend`-phrases.

### Changed
- _Requests_ - The 'handshake'-response has underscores in its field names replaced by dashes \[**breaking change**\].
   - This affects `supported_versions`, `supported_extensions`, `reasoner_version`, `shares_updates`, `shares_triggers` and `shares_violations`.
- _Requests_ - The 'phrases'-request now has a toplevel **phrase-group** instead of a **phrase** to model the transactional model of the new eFLINT.
   - Note this is not a breaking change, as the syntax allows individual phrases to be given instead of a list.
- _Types_ - The predicate-variant of the **phrase**-type now uses `is-invariant` instead of `is_invariant` \[**breaking change**\].
- _Types_ - **trigger**-types explicitly embed nested constructor applications instead of separate fields \[**breaking change**\].
- _Types_ - **trigger**-types now report their parents as constructor applications, to capture types \[**breaking change**\].
- _Types_ - **trigger**-types have specific text that permits servers to permute them as they see fit to hide information.
- _Types_ - There is now a specific list of fields allowed to be extended with `Extend` embedded in the specification \[**breaking change**\].
- _Appendix_ - Proposed extensions have an _Extension version_ that advertises which version of the extension is being proposed.
- _Appendix_ - The instances-extension is now bumped to 2.0.0.
- _Appendix_ - Added 'WHERE' as a synonym for 'WHEN'.
- This repository now hosts the LaTeX source files.
   - As such, the examples are made available separately in the [`Examples/`](./Examples/)-directory.

### Added - Instances extension
- Added the **authentication**-type, which is used to differentiate between different authentication methods.
   - Added the 'none'-variation.
   - Added the 'password'-variation.
- Added the **authentication-kind**-type, which is used to communicate which authentication scheme is used -and which settings are used- to clients.
   - Added the 'none'-variation.
   - Added the 'password'-variation.

### Changed - Instances extension
- Replaced the `password`-field with an `authentication`-field, which is more general (breaking change).
   - This changes the 'instance-new' and 'instance-del' requests.
- Replaced the `is-public`-field with the `authentication`-field, which also encodes which authentication scheme is used (breaking change).
   - This changes the **instance**-type.
- Replaced the `instance-password`-field in the phrases-request extension with `instance-authentication` to match previous changes (breaking change).



## v2.0.0 - 2024-02-15
### Added
- The `instances` proposed extension that can be used to select different states per request, some of which may even require a password to access.

### Changed
- The way extensions are communicated to the server. They are no longer part of the version number, but instead have an additional toplevel `extensions`-field that permits users more control, including extension version selection \[**breaking change**\].

### Fixed
- A few typos here and there.


## v1.0.0 - 2023-06-23
### Added
- (Almost) complete support for eFLINT 3. Only the contentious Where/When clauses are not included, as well as `Created by`, `Terminated by` and `Obfuscated by`.


## v0.1.0 - 2023-01-18
Initial release.
