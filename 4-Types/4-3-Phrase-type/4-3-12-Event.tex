\subsubsection{event variant} \label{sec:phrase-event}
\textit{Phrase identifier: "event"}\\
\textit{Phrase subclass: Definition}

To explicitly model transitions in the reasoner's knowledge base, eFLINT has a concept of Events. The transition they model is a transition of truth values of other objects, and can thus be triggered to make these transitions happen (see section \ref{sec:phrase-trigger}). Such an event is represented by the \textbf{event}-variant.

The JSON schema for an \textbf{event}-variant has the following fields:
\begin{itemize}
    \item {
        \texttt{name}: A JSON String that contains the name (i.e., identifier) for the new Event.
    }
    \item {
        \texttt{related-to}: An \textit{optional} JSON Array of zero or more JSON Strings that describes any other parameters for the Event, much in the same way as the \texttt{identified-by} field does for the \textbf{fact}-type (see section \ref{sec:phrase-type}). If omitted, will default to an empty JSON Array.\\
        Note, however, that related-to may not use primitive types (e.g., "String" or "Int" in eFLINT). Instead, use user-defined types (for eFLINT, one may use the builtin "string" or "int" types instead).
    }
    % \item {
    %     \texttt{derived-from}: An \textit{optional} JSON Array of \textbf{derived-from}-types (see section \ref{sec:derived-from-type}) that attaches a derivation rule to an Event. At least one such rule should hold true for the Event to be derived. If omitted, will default to an empty array, implying that the Event cannot be derived (unless \texttt{holds-when} is given; see below).
    % }
    % \item {
    %     \texttt{holds-when}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that automatically derives the truth value of an Event based on some predicate (the expressions given). Note that this a different variant of a derivation clause, and thus either a derivation rule \textit{or} a holds-when predicate needs to evaluate to 'true' for the Event to be derived. If omitted, will default to an empty array, implying that the Event cannot be derived (as long as \texttt{derived-from} is also empty).
    % }
    % \item {
    %     \texttt{conditioned-by}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that automatically derives the truth value of an Event based on some predicate (the expressions given). However, in contrast to the \texttt{holds-when} field, \textit{all} predicates given by the \texttt{conditioned-by} field have to evaluate to true before the Event can be derived. This implies that a Event cannot be derived by conditions alone; instead, it merely imposes additional restrictions besides any derivation clauses. If omitted, will default to an empty array, implying that there are no conditions for deriving this Event.
    % }
%     \item {
%         \texttt{derived-from}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that defines how to derive new instances of this Event based on the reasoner's state. The expressions should thus be instance expressions, generating the new instances. If omitted, will default to an empty array, implying that the Event cannot be derived (unless \texttt{holds-when} is given; see below).
%     }
%     \item {
%         \texttt{holds-when}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that automatically derives the truth value of a Event based on some predicate (the expressions given). Note that both the \texttt{derived-from} and \texttt{holds-when} fields may derive new instances; concretely, this means that an Event may be derived if there is a single \texttt{derived-from} \textit{or} \texttt{holds-when} rule that allows it to be.\\
%         The expressions must be boolean expressions. If this field is omitted, will default to an empty array, implying that the Event cannot be derived (as long as \texttt{derived-from} is also empty).
%     }
%     \item {
%         \texttt{conditioned-by}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that restricts the types derived with the \texttt{derived-from} and \texttt{holds-when} fields. In particular, for an Event to be derived, it has to satisfy the conditions of \textit{all} \texttt{conditioned-by} rules it has (i.e., they are conjunctive).\\
%         The expressions must be boolean expressions. If this field is omitted, it will default to an empty array, implying that there are no additional conditions for deriving this Event.
%     }
%     \item {
%         \texttt{where}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that restricts the types that can exist. In particular, for an Event to be derived, it has to satisfy the conditions of \textit{all} \texttt{conditioned-by} rules it has (i.e., they are conjunctive).\\
%         A \texttt{where} is stronger than a \texttt{conditioned-by} in that conditions imposed by \texttt{where}-clauses also restrict postulation in addition to just derivation. Consider the following example:
%         \begin{lstlisting}[language=eflint]
% // Create citizens that can have ages
% Fact citizen Identified by string.
% Fact age Identified by citizen * int.
% Fact voting-age Identified by age.
%     Holds when age(citizen', int')
%     Conditioned by int >= 18.
% Fact voting-age-strict Identified by citizen * int.
%     Holds when age(citizen', int')
%     Where int >= 18.

% // Check if Amy can be 8 and she is derived to have voting
% // age
% +citizen(Amy).
% +age(citizen(Amy), 8).
% ?voting-age(age(citizen(Amy), 8)).          // False
% ?voting-age-strict(age(citizen(Amy), 8)).   // False

% // Posulate Amy has a voting age; then check again
% +voting-age(age(citizen(Amy, 8))).
% +voting-age-strict(age(citizen(Amy, 8))).
% ?voting-age(age(citizen(Amy), 8)).          // False
% ?voting-age-strict(age(citizen(Amy), 8)).   // False
%         \end{lstlisting}
%         The expressions in the \texttt{when}-field must be boolean expressions. If this field is omitted, it will default to an empty array, implying that there are no additional conditions for deriving or postulating this Event.
%     }
%     \item {
%          \texttt{when}: Alias for the \texttt{where}-field.
%     }
    \input{4-Types/4-3-Phrase-type/snippets/4-3-Derived-from-Holds-when-Conditioned-by-Where-When}
    \item {
        \texttt{syncs-with}: An \textit{optional} JSON Array of zero or more \textbf{expression}-types (see section \ref{sec:expression-type}) that will trigger any other event or act when this event triggers. Thus, the expressions should be instance expressions that generate all the events and acts that will be triggered.
    }
    \item {
        \texttt{creates}: An \textit{optional} JSON Array of zero or more \textbf{expression}-types (see section \ref{sec:expression-type}) listing the objects to create (i.e., assign a truth value of 'true'). The expressions should be instance expressions, each of which can evaluate to zero or more instances.\\
        If omitted, then this is an empty array.
    }
    \item {
        \texttt{terminates}: An \textit{optional} JSON Array of zero or more \textbf{expression}-types (see section \ref{sec:expression-type}) listing the objects to terminate (i.e., assign a truth value of 'false'). The expressions should be instance expressions, each of which can evaluate to zero or more instances.\\
        If omitted, then this is an empty array.
    }
    \item {
        \texttt{obfuscates}: An \textit{optional} JSON Array of zero or more \textbf{expression}-types (see section \ref{sec:expression-type}) listing the objects to obfuscate (i.e., remove their assignment altogether). The expressions should be instance expressions, each of which can evaluate to zero or more instances.\\
        If omitted, then this is an empty array.
    }
\end{itemize}

A few examples of such a JSON schema would be as follows:
\lstinputlisting[language=json]{Examples/4-Types/4-3-Phrase-type/4-3-12-Event.json}
