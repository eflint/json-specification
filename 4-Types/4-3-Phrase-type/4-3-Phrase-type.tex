\subsection{phrase type} \label{sec:phrase-type}
A \textbf{phrase}-type represents the possible phrases that may build an eFLINT program. They can be thought of as the smallest, semantically valid, non-zero programs one can write.

We can distinguish three different categories of phrases: queries, which are used to query the current state of the reasoner (sections \ref{sec:query-start} through \ref{sec:query-end}); statements, which are used to change the current state of the reasoner (sections \ref{sec:statement-start} through \ref{sec:statement-end}); and definitions, which are used to extend the possible states of the reasoners by defining new types (sections \ref{sec:definition-start} through \ref{sec:definition-end}).

Because there are so many different phrases, the \textbf{phrase}-type itself consists of multiple variants. However, all of them have the following fields in common:
\begin{itemize}
    \item {
        \texttt{kind}: A JSON String with an identifier describing the kind of the phrase. This determines the exact variant of the phrase. The possible values are defined in the sections regarding the variants (see below).
    }
    \item {
        \texttt{stateless}: An \textit{optional} JSON boolean that indicates whether the result of this phrase should be discarded after this request or not. Specifically, if this value is true, then any state-changing effect of the phrase will be reverted after all the phrases in the request have been processed. If false, then the state-change is preserved. If omitted, then it is interpreted as 'false'.\\
        Note that for queries (sections \ref{sec:query-start} through \ref{sec:query-end}), the \texttt{stateless}-field has no effect because queries are always stateless. This is useful to allow an optimisation opportunity for the server when executing the queries.\\
        Note that the property that some phrases may be stateless and others note breaks the sequential property\cite{sequential} of the language. See section \ref{sec:appendix-extensions-sequential} in the Appendix for more information and ways to work with this.
    }
    \item {
        \texttt{updates}: An \textit{optional} JSON boolean that indicates whether the client is open to receiving updates and violations that this phrase caused. Specifically, if this value is true, then the server \textit{may} populate the \texttt{changes}, \texttt{triggers} and \texttt{violations} fields in the \textbf{phrase-result} type (see section \ref{sec:phrase-result-type}); but it may also still omit them for security or privacy reasons. Alternatively, if the value is false, then the server is forbidden from populating those fields. This is mostly useful as an optimisation, to save bandwidth if the client is not interested in receiving the updates anyway.\\
        There is also a toplevel \texttt{updates}-field in the phrases-request that contains the default setting for all phrases. Thus, if this field is omitted, it may be assumed to have the same value as the toplevel field. Otherwise, only this value should be leading.
    }
\end{itemize}
This is very similar to how the \textbf{statement}-type behaves.

In subsequent subsections, we will go through the possible variants of the \textbf{phrase}-type. Every subsection starts by defining the variant's \textit{phrase identifier}, which is the value for the \texttt{kind}-field that defines that variant, and its \textit{phrase subclass}, which may be used to reference to groups of phrases in other places of the specification.

\input{4-Types/4-3-Phrase-type/4-3-1-Boolean-query}
\input{4-Types/4-3-Phrase-type/4-3-2-Instance-query}

\input{4-Types/4-3-Phrase-type/4-3-3-Create}
\input{4-Types/4-3-Phrase-type/4-3-4-Terminate}
\input{4-Types/4-3-Phrase-type/4-3-5-Obfuscate}

\input{4-Types/4-3-Phrase-type/4-3-6-Trigger}
\input{4-Types/4-3-Phrase-type/4-3-7-Atomic-fact}
\input{4-Types/4-3-Phrase-type/4-3-8-Composite-fact}
\input{4-Types/4-3-Phrase-type/4-3-9-Function-fact}
\input{4-Types/4-3-Phrase-type/4-3-10-Placeholder}
\input{4-Types/4-3-Phrase-type/4-3-11-Predicate-Invariant}
\input{4-Types/4-3-Phrase-type/4-3-12-Event}
\input{4-Types/4-3-Phrase-type/4-3-13-Act}
\input{4-Types/4-3-Phrase-type/4-3-14-Duty}
\input{4-Types/4-3-Phrase-type/4-3-15-Extend}
