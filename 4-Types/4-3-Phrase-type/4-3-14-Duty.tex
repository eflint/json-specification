\subsubsection{duty variant} \label{sec:phrase-duty} \label{sec:definition-almost-end}
\textit{Phrase identifier: "duty"}\\
\textit{Phrase subclass: Definition}

The \textbf{duty}-variant implements an eFLINT Duty, which captures that an agent has to take one of a number of alternative actions (the terminating actions of that duty). To prevent violating a duty, an actor has to perform one of the terminating actions once the duty is enabled, before the duty is violated (as indicated by predicates).

The truth value assigned to a Duty determines whether it is enabled, and its (violation-)clause determines when the agent has failed to satisfy the Duty.

The fields in a \textbf{duty}-variant directly correlate to those in the eFLINT spec:
\begin{itemize}
    \item {
        \texttt{name}: A JSON String carrying the name (i.e., identifier) of the new Duty.
    }
    \item {
        \texttt{holder}: A JSON String that describes the agent that has to satisfy the Duty. Its value should equal the identifier of the referenced agent (i.e., a Fact).
    }
    \item {
        \texttt{claimant}: A JSON String that describes the agent who imposes the Duty on the holder. Its value should equal the identifier of the referenced agent (i.e., a Fact).
    }
    \item {
        \texttt{related-to}: An \textit{optional} JSON Array of zero or more JSON Strings that describes any other parameters for the Duty, much in the same way as the \texttt{identified-by} field does for the \textbf{fact}-type (see section \ref{sec:phrase-type}). If omitted, will default to an empty JSON Array.\\
        Note, however, that related-to may not use primitive types (e.g., "String" or "Int" in eFLINT). Instead, use user-defined types (for eFLINT, one may use the builtin "string" or "int" types instead).
    }
    % \item {
    %     \texttt{derived-from}: An \textit{optional} JSON Array of \textbf{derived-from}-types (see section \ref{sec:derived-from-type}) that attaches a derivation rule to a Duty. At least one such rule should hold true for the Duty to be derived. If omitted, will default to an empty array, implying that the Duty cannot be derived (unless \texttt{holds-when} is given; see below).
    % }
    % \item {
    %     \texttt{holds-when}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that automatically derives the truth value of a Duty based on some predicate (the expressions given). Note that this a different variant of a derivation clause, and thus either a derivation rule \textit{or} a holds-when predicate needs to evaluate to 'true' for the Duty to be derived. If omitted, will default to an empty array, implying that the Duty cannot be derived (as long as \texttt{derived-from} is also empty).
    % }
    % \item {
    %     \texttt{conditioned-by}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that automatically derives the truth value of an Duty based on some predicate (the expressions given). However, in contrast to the \texttt{holds-when} field, \textit{all} predicates given by the \texttt{conditioned-by} field have to evaluate to true before the Duty can be derived. This implies that a Duty cannot be derived by conditions alone; instead, it merely imposes additional restrictions besides any derivation clauses. If omitted, will default to an empty array, implying that there are no conditions for deriving this Duty.
    % }
%     \item {
%         \texttt{derived-from}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that defines how to derive new instances of this Duty based on the reasoner's state. The expressions should thus be instance expressions, generating the new instances. If omitted, will default to an empty array, implying that the Event cannot be derived (unless \texttt{holds-when} is given; see below).
%     }
%     \item {
%         \texttt{holds-when}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that automatically derives the truth value of a Duty based on some predicate (the expressions given). Note that both the \texttt{derived-from} and \texttt{holds-when} fields may derive new instances; concretely, this means that a Duty may be derived if there is a single \texttt{derived-from} \textit{or} \texttt{holds-when} rule that allows it to be.\\
%         The expressions must be boolean expressions. If this field is omitted, will default to an empty array, implying that the Duty cannot be derived (as long as \texttt{derived-from} is also empty).
%     }
%     \item {
%         \texttt{conditioned-by}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that restricts the types derived with the \texttt{derived-from} and \texttt{holds-when} fields. In particular, for a Duty to be derived, it has to satisfy the conditions of \textit{all} \texttt{conditioned-by} rules it has (i.e., they are conjunctive).\\
%         The expressions must be boolean expressions. If this field is omitted, it will default to an empty array, implying that there are no additional conditions for deriving this Duty.
%     }
%     \item {
%         \texttt{where}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that restricts the types that can exist. In particular, for a Duty to be derived, it has to satisfy the conditions of \textit{all} \texttt{conditioned-by} rules it has (i.e., they are conjunctive).\\
%         A \texttt{where} is stronger than a \texttt{conditioned-by} in that conditions imposed by \texttt{where}-clauses also restrict postulation in addition to just derivation. Consider the following example:
%         \begin{lstlisting}[language=eflint]
% // Create citizens that can have ages
% Fact citizen Identified by string.
% Fact age Identified by citizen * int.
% Fact voting-age Identified by age.
%     Holds when age(citizen', int')
%     Conditioned by int >= 18.
% Fact voting-age-strict Identified by citizen * int.
%     Holds when age(citizen', int')
%     Where int >= 18.

% // Check if Amy can be 8 and she is derived to have voting
% // age
% +citizen(Amy).
% +age(citizen(Amy), 8).
% ?voting-age(age(citizen(Amy), 8)).          // False
% ?voting-age-strict(age(citizen(Amy), 8)).   // False

% // Posulate Amy has a voting age; then check again
% +voting-age(age(citizen(Amy, 8))).
% +voting-age-strict(age(citizen(Amy, 8))).
% ?voting-age(age(citizen(Amy), 8)).          // False
% ?voting-age-strict(age(citizen(Amy), 8)).   // False
%         \end{lstlisting}
%         The expressions in the \texttt{when}-field must be boolean expressions. If this field is omitted, it will default to an empty array, implying that there are no additional conditions for deriving or postulating this Duty.
%     }
%     \item {
%          \texttt{when}: Alias for the \texttt{where}-field.
%     }
    \input{4-Types/4-3-Phrase-type/snippets/4-3-Derived-from-Holds-when-Conditioned-by-Where-When}
    \item {
        \texttt{violated-when}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that determines when this Duty is violated. Specifically, it is a boolean expression, and when it holds true \textit{while} the Duty itself holds true (i.e., it is enabled), eFLINT will consider the Duty to be violated.\\
        The violations are disjunctive, i.e., if any of the expressions holds true, then the Duty is considered to be violated even if the others evaluate to false.\\
        If omitted, then this will default to an empty JSON Array, meaning it will never be violated.
    }
    \item {
        \texttt{enforced-by}: An \textit{optional} JSON Array of JSON strings that denotes a list of types. Whenever one of these types holds true (i.e., in the case of events, acts and duties, if it is enabled), the Duty is considered violated.\\
        This is essentially syntax sugar for a \texttt{violated-when} entry that checks if the given type is enabled for the same parameters as the Duty. So, for example, a Duty with an \texttt{enforced-by} clause of the shape:
        \begin{lstlisting}[language=eflint]
Duty must-vote
    Holder citizen
    Claimant administrator
    Enforced by not-voted.
        \end{lstlisting}
        would desugar to something like:
        \begin{lstlisting}[language=eflint]
Duty must-vote
    Holder citizen
    Claimant administrator
    Violated when Enabled(
        not-voted(citizen, administrator)
    ).
        \end{lstlisting}
        (see section \ref{sec:appendix-eflint-expressions-miscellaneous-operators} for the Enabled-operator)\\
        Note that this behaviour limits the types that can be used in \texttt{enforced-by}: only types with the exact same fields (with the same names!) as the enforced Duty are valid.\\
        Because the \texttt{enforced-by} field is syntax sugar for \texttt{violated-when}, it is disjunctive both with itself and other \texttt{violated-when} clauses.\\
        If this field is omitted, it will default to an empty array, implying no additional conditions for violating the duty.
    }
\end{itemize}

A few examples of a \textbf{duty}-variant are:
\lstinputlisting[language=json]{Examples/4-Types/4-3-Phrase-type/4-3-14-Duty.json}
