\subsubsection{composite-fact variant} \label{sec:phrase-composite-fact}
\textit{Phrase identifier: "cfact"}\\
\textit{Phrase subclass: Definition}

The \textbf{composite-fact} variant is one of the two ways to define a regular eFLINT Fact. That means that any instance of this definition will have a truth value when queried, and that its truth value may automatically be derived based on the reasoner's state by annotating it with derivation rules.

This variant defines a composite Fact, which has one or more parameters. The domain of these Facts is always open, and is equal to the domain of each of its parameters.

To accommodate this, the \textbf{composite-fact} variant defines the following fields:
\begin{itemize}
    \item {
        \texttt{name}: The name of the new Fact-type, as a JSON String.
    }
    \item {
        \texttt{identified-by}: A JSON Array of one or more JSON Strings that define the 'parameters' of the Fact. Every element in the array references the identifier of a specific type that this Fact uses as one of its parameters.\\
        If you intend to use primitive types as one of the parameters, use a composite type variant of that to make sure the eFLINT reasoner does not assume it's an atomic fact (e.g., use "string" instead of "String").
    }
    \item {
        \texttt{is-var}: An \textit{optional} JSON boolean that determines if this fact is actually a variable.\\
        If it is, that means that only \textit{one} instance of the fact can exist at a time. If any new ones are created, the reasoner will automatically remove the old one(s).\\
        If omitted, the \texttt{is-var} should default to false.
    }
%     \item {
%         \texttt{derived-from}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that defines how to derive new instances of this Fact based on the reasoner's state. The expressions should thus be instance expressions, generating the new instances. If omitted, will default to an empty array, implying that the Fact cannot be derived (unless \texttt{holds-when} is given; see below).
%     }
%     \item {
%         \texttt{holds-when}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that automatically derives the truth value of a Fact based on some predicate (the expressions given). Note that both the \texttt{derived-from} and \texttt{holds-when} fields may derive new instances; concretely, this means that a Fact may be derived if there is a single \texttt{derived-from} \textit{or} \texttt{holds-when} rule that allows it to be.\\
%         The expressions must be boolean expressions. If this field is omitted, will default to an empty array, implying that the Fact cannot be derived (as long as \texttt{derived-from} is also empty).
%     }
%     \item {
%         \texttt{conditioned-by}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that restricts the types derived with the \texttt{derived-from} and \texttt{holds-when} fields. In particular, for a Fact to be derived, it has to satisfy the conditions of \textit{all} \texttt{conditioned-by} rules it has (i.e., they are conjunctive).\\
%         The expressions must be boolean expressions. If this field is omitted, it will default to an empty array, implying that there are no additional conditions for deriving this Fact.
%     }
%     \item {
%         \texttt{where}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that restricts the types that can exist. In particular, for a Fact to be derived, it has to satisfy the conditions of \textit{all} \texttt{conditioned-by} rules it has (i.e., they are conjunctive).\\
%         A \texttt{where} is stronger than a \texttt{conditioned-by} in that conditions imposed by \texttt{where}-clauses also restrict postulation in addition to just derivation. Consider the following example:
%         \begin{lstlisting}[language=eflint]
% // Create citizens that can have ages
% Fact citizen Identified by string.
% Fact age Identified by citizen * int.
% Fact voting-age Identified by age.
%     Holds when age(citizen', int')
%     Conditioned by int >= 18.
% Fact voting-age-strict Identified by citizen * int.
%     Holds when age(citizen', int')
%     Where int >= 18.

% // Check if Amy can be 8 and she is derived to have voting
% // age
% +citizen(Amy).
% +age(citizen(Amy), 8).
% ?voting-age(age(citizen(Amy), 8)).          // False
% ?voting-age-strict(age(citizen(Amy), 8)).   // False

% // Postulate Amy has a voting age; then check again
% +voting-age(age(citizen(Amy, 8))).
% +voting-age-strict(age(citizen(Amy, 8))).
% ?voting-age(age(citizen(Amy), 8)).          // False
% ?voting-age-strict(age(citizen(Amy), 8)).   // False
%         \end{lstlisting}
%         The expressions in the \texttt{when}-field must be boolean expressions. If this field is omitted, it will default to an empty array, implying that there are no additional conditions for deriving or postulating this Fact.
%     }
%     \item {
%          \texttt{when}: Alias for the \texttt{where}-field.
%     }
    \input{4-Types/4-3-Phrase-type/snippets/4-3-Derived-from-Holds-when-Conditioned-by-Where-When}
\end{itemize}

The following JSON snippets are examples of valid composite fact-definitions:
\lstinputlisting[language=json]{Examples/4-Types/4-3-Phrase-type/4-3-8-Composite-fact.json}
