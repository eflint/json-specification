\subsection{expression type} \label{sec:expression-type}
eFLINT supports expressions, which can be used to express complex queries that depend on multiple truth-values. To express these in a JSON Schema, the \textbf{expression}-type uses six different variants.

Generally, expressions come in two forms: either as \textit{instance expressions}, which evaluate to zero or more instances of defined types, or \textit{boolean expressions}, which evaluate to a simple true or false. There are a few other expressions like the boolean expressions but for other types (i.e., \textit{string expressions} and \textit{integer expressions}); but these rarely occur, and if they do, usually only in a nested expression.

The following six subsections discuss each of the expression variants.

\subsubsection{Primitive values (variant 1)} \label{sec:expression-primitive-type}
The first variant of the \textbf{expression}-type simply represents primitive values in eFLINT. These are strings (represented by a single JSON String), integers (represented by a single JSON number) or booleans (represented by a single JSON boolean). Consequently, they also have trivial evaluation, evaluating to the same type as the primitive used.

A few examples:
\lstinputlisting[language=json]{Examples/4-Types/4-6-Expression-type/4-6-1-Primitive-values.json}

\subsubsection{Variable reference (variant 2)} \label{sec:expression-variable-reference}
The second variant of the \textbf{expression}-type defines a variable reference. This is simply an identifier that refers to a previously defined variable or instance of that variable. Most commonly, variable references are used to refer to variables bound by quantifiers, where we want to explicitly disambiguate between the type that may have the same name and the variable itself.

The evaluation of the variable reference is dependent on the type of the expression. If the variable reference is used in a boolean expression, it evaluates to the truth value of the referenced instance. Otherwise, if it's used in an instance expression, it evaluates to the referenced instance itself.

To avoid ambiguity with the first variant, the variable reference is represented as a JSON Array of exactly one JSON String, which contains the identifier of the variable referenced. For example:
\lstinputlisting[language=json]{Examples/4-Types/4-6-Expression-type/4-6-2-Variable-reference.json}

\subsubsection{Constructor application (variant 3)} \label{sec:expression-constructor-application}
The third variant of the \textbf{expression}-type expresses the \textit{constructor application}, which instantiates a composite type (specific definitions of Facts, Events, Acts, ...). These always evaluate to instances, which has the same type of the type constructed by the constructor application. It uses the following JSON Schema:
\begin{itemize}
    \item {
        \texttt{identifier}: A JSON String describing the composite type for which to construct a value. Its value should match the value in the \texttt{identifier}-field of the definition (sections \ref{sec:definition-start} to \ref{sec:definition-end}) that is referenced here.
    }
    \item {
        \texttt{operands}: A \textbf{constructor-input} type (see section \ref{sec:constructor-input-type}) that specifies the values of the fields for the constructed type. If the array syntax (section \ref{sec:constructor-input-array-syntax}) is used, the size of the array must be \textit{equal} to the number of fields in the constructed type (special fields like \texttt{actor} or \texttt{claimant} included); alternatively, if the map syntax (section \ref{sec:constructor-input-array-syntax}) syntax is used, the number of elements in the map must \textit{not exceed} the number of fields in the constructed type.
    }
\end{itemize}

A few examples:
\lstinputlisting[language=json]{Examples/4-Types/4-6-Expression-type/4-6-3-Constructor-application.json}

\subsubsection{Operators (variant 4)} \label{sec:expression-operators}
The fourth variant of the \textbf{expression}-type defines an operation, which provides a non-trivial evaluation of its operands based on the operator used. It uses a JSON Schema with two fields:
\begin{itemize}
    \item {
        \texttt{operator}: An \textbf{operator}-type (section \ref{sec:operator-type}) that expresses which operator is applied.
    }
    \item {
        \texttt{operands}: A JSON Array of zero or more (recursive) \textbf{expression}-types detailing each of the operands. The length of this list should match the number of operands accepted by the chosen operator.
    }
\end{itemize}

An example that shows the fourth variant:
\lstinputlisting[language=json]{Examples/4-Types/4-6-Expression-type/4-6-4-Operators.json}

\subsubsection{Iterator expressions (variant 5)} \label{sec:expression-iterator-expressions}
The fifth variant of the \textbf{expression}-type defines special iterator operators (e.g., Foreach, Forall, Exists, ...). These are operators that work over a domain of variables, which are then analysed according to some predicate. They will either evaluate to (a list of) instances (Foreach) or a boolean value (Forall, Exists). The JSON schema used is:
\begin{itemize}
    \item {
        \texttt{iterator}: A JSON String describing the operation to execute. The list of accepted operators for the eFLINT reasoner is given in the Appendix.
    }
    \item {
        \texttt{binds}: A JSON Array of zero or more identifiers (as JSON Strings) of variables bound in the expression of this \textbf{iterator}. Note that all of these are new identifiers; even if the identifier is already used in an existing variable, the underlying backend should shadow that declaration and bind its identifier temporarily as a free variable.\\
        Also note that, if the underlying reasoner supports it (e.g., eFLINT), variables may also be implicitly bound. Put differently: this list may not be exhaustive of all the variables that the iterator expression binds.
    }
    \item {
        \texttt{expression}: A (recursive) \textbf{expression}-type that is used as the expression for this iterator. Its role depends on the specific operator.\\
        For Foreach, the expression is an instance expression, and will determine the instance that is generated per instance that is iterated (i.e., it defines a mapping from each input instance to an output instance).\\
        For other operators (Forall, Exists), the expression is a boolean expression, and is used to check if any of the iterated instances matches the given predicate. Based on the operator, this generates list of boolean values determines the result of the iterator expression.
    }
\end{itemize}

An example that shows an iterator expression:
\lstinputlisting[language=json]{Examples/4-Types/4-6-Expression-type/4-6-5-Iterator-expressions.json}

\subsubsection{Projection expressions (variant 6)} \label{sec:expression-projection}
Finally, the sixth variant of the \textbf{expression}-type contributes \textit{projection}. This is very much like normal operators (variant 4), except that the field name we are projecting is passed separately, as this is not an expression at all.

It uses a JSON schema with the following fields:
\begin{itemize}
    \item {
        \texttt{parameter}: A JSON String describing the field to project upon the operand. Which fields are allowed are, of course, depending on the type we are projecting on.
    }
    \item {
        \texttt{operand}: A single \textbf{expression}-type detailing what we are projecting on. This should evaluate to a single composite type.
    }
\end{itemize}

An example that shows the sixth variant:
\lstinputlisting[language=json]{Examples/4-Types/4-6-Expression-type/4-6-6-Projection-expressions.json}
