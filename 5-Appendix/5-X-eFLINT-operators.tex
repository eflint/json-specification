\subsection{eFLINT operators} \label{sec:appendix-eflint-expressions}
In this section, we list eFLINT-specific operators and other expressions supported by eFLINT.

We will first treat all the normal operators that occur in the operator-variant of the \textbf{expression}-type (section \ref{sec:expression-operators}). Then, in subsection \ref{sec:appendix-eflint-expressions-iterator-variant-operators}, we treat operators that are specific to the iterator-variant of the \textbf{expression}-type (section \ref{sec:expression-iterator-expressions}).

Note that this list is not part of the specification itself, but rather of the eFLINT language. For an up-to-date list, check the eFLINT documentation instead. This also means that any changes to this list are \textit{not} reflected by an update to the Specification version number.

\subsubsection{Logical operators} \label{sec:appendix-eflint-expressions-logical-operators}
\begin{itemize}
    \item {
        Conjunction: identified by 'AND', takes the logical conjunction of multiple boolean values. eFLINT allows 0 or more operands for this type, since it is associative; in that case, the result will be a conjunction over all operands. If there are 0 elements, then the conjunction always returns 'true'.

        A conjunction always evaluates to a boolean value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-1-Conjunction.json}
    }
    \item {
        Disjunction: identified by 'OR', takes the logical disjunction of multiple boolean values. eFLINT allows 0 or more operands for this type, since it is associative; in that case, the result will be a disjunction over all operands. If there are 0 elements, then the conjunction always returns 'false'.

        A disjunction always evaluates to a boolean value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-2-Disjunction.json}
    }
    \item {
        Negation: identified by 'NOT', takes the logical negation of a single boolean value. Consequently, it accepts exactly one operand.

        Negations always evaluate to boolean values.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-3-Negation.json}
    }
\end{itemize}


\subsubsection{Comparison operators} \label{sec:appendix-eflint-expressions-comparison-operators}
\begin{itemize}
    \item {
        Equality: identified by 'EQ', returns true iff both of its operands evaluate to the same value. It accepts exactly 2 operands.

        An equality always evaluates to a boolean value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-4-Equality.json}
    }
    \item {
        Inequality: identified by 'NE', returns true iff its two operands evaluate to different values. It accepts exactly 2 operands.

        An inequality always evaluates to a boolean value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-5-Inequality.json}
    }
    \item {
        Greater-than: identified by 'GT', returns true iff its first operand is (strictly) larger than its second. It accepts exactly two operands.

        A greater-than operator always evaluates to a boolean value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-6-Greater-than.json}
    }
    \item {
        Greater-than-or-equal: identified by 'GE', returns true iff its first operand is larger than or equal to its second. It accepts exactly two operands.

        A greater-than-or-equal operator always evaluates to a boolean value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-7-Greater-than-or-equal.json}
    }
    \item {
        Less-than: identified by 'LT', returns true iff its first operand is (strictly) smaller than its second. It accepts exactly two operands.

        A less-than operator always evaluates to a boolean value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-8-Less-than.json}
    }
    \item {
        Less-than-or-equal: identified by 'LE', returns true iff its first operand is smaller than or equal to its second. It accepts exactly two operands.

        A less-than-or-equal operator always evaluates to a boolean value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-9-Less-than-or-equal.json}
    }
\end{itemize}


\subsubsection{Arithmetic operators} \label{sec:appendix-eflint-expressions-arithmetic-operators}
\begin{itemize}
    \item {
        Addition: identified by 'ADD', the addition is used to add two numbers together. Thus, it also accepts exactly two operands.

        An addition always evaluates to a numerical value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-10-Addition.json}
    }
    \item {
        Subtraction: identified by 'SUB', the subtraction is used two subtract numbers from each other. Thus, it also accepts exactly two operands.

        A subtraction always evaluates to a numerical value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-11-Subtraction.json}
    }
    \item {
        Multiplication: identified by 'MUL', the multiplication is used to multiply two numbers together. Thus, it also accepts exactly two operands.

        A multiplication always evaluates to a numerical value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-12-Multiplication.json}
    }
    \item {
        Division: identified by 'DIV', the division is used to divide two numbers by each other. Thus, it also accepts exactly two operands.

        A division always evaluates to a numerical value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-13-Division.json}
    }
    \item {
        Modulo: identified by 'MOD', the modulo-operator is used to take the modulo of two numbers. Thus, it also accepts exactly two operands.

        A modulo always evaluates to a numerical value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-14-Modulo.json}
    }
\end{itemize}


\subsubsection{Iterator operators} \label{sec:appendix-eflint-expressions-iterator-operators}
\begin{itemize}
    \item {
        Count operator: identified by 'COUNT', the count operator is used to count the number of instances in an instance expression. It thus takes only one operand, which is the instance expression that generates the list. It doesn't matter to what type the instances evaluate.

        A count operator always evaluates to a number.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-15-Count.json}
    }
    \item {
        Summation: identified by 'SUM', the summation operator is used to add the evaluated values of all instances in the given instance expression together. This must be an instance expression that generates zero or more instances, and all instances must evaluate to a number.

        A summation operator always evaluates to a number.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-16-Sum.json}
    }
    \item {
        Max: identified by 'MAX', the max operator returns the largest numerical value from an instance expression that generates numerical values.

        A max operator always evaluates to a number.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-17-Max.json}
    }
    \item {
        Min: identified by 'MIN', the min operator returns the smallest numerical value from an instance expression that generates numerical values.

        A min operator always evaluates to a number.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-18-Min.json}
    }
\end{itemize}


\subsubsection{Miscellaneous operators} \label{sec:appendix-eflint-expressions-miscellaneous-operators}
\begin{itemize}
    \item {
        When operator: identified by 'WHEN' \textit{or} 'WHERE', the when-operator is used to 'remove' an instance. Specifically, it takes an instance expression and a boolean expression, and when the latter evaluates to 'true', the former is returned. However, if the expression evaluates to 'false', then the instance is \textit{not} returned. This is particularly useful when working with iterators.

        A When-operator either evaluates to the given instance, or to nothing at all.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-19-When-Where.json}
    }
    \item {
        Holds operator: identified by 'HOLDS', the holds-operator essentially implements a boolean query (see section \ref{sec:phrase-boolean-query}) in expression position. It takes an instance expression that evaluates to a single instance, and outputs the truth value of that instance.
?
        A Holds-operator thus evaluates to a boolean value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-20-Holds.json}
    }
    \item {
        Enabled operator: identified by 'ENABLED', the enabled-operator can be used to check if an instantiation \textit{can} be derived based on any conditions it has. It takes an instance expression that evaluates to a single instance, and outputs whether that instance has been, or could have been, derived.

        A Enabled-operator evaluates to a boolean value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-21-Enabled.json}
    }
    \item {
        Violated operator: identified by 'VIOLATED', the violated-operator can be used to check if an instantiation would trigger any violations. It takes an instance expression that evaluates to a single instance, and outputs whether that instance causes violation.

        A Violated-operator evaluates to a boolean value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-22-Violated.json}
    }
\end{itemize}


\subsubsection{Iterator variant operators} \label{sec:appendix-eflint-expressions-iterator-variant-operators}
These operators must be specified in the iterator-variant of the \textbf{expression}-type (section \ref{sec:expression-iterator-expressions}) instead of the operator-variant.
\begin{itemize}
    \item {
        Foreach operator: the foreach operator is identified by 'FOREACH', and is used to transform one domain of instances to another. Specifically, it allows one to implement a mapping from one instance type to another by providing an expression that constructs or retrieves the proper instance. In combination with the 'Where'-operator, this becomes even more powerful, as one can also filter domains.\\
        The foreach operator takes a number of identifiers that are 'bound': this means that the foreach will iterate over the product f all the bound types' domains (this is not entirely true; refer to eFLINT's documentation). Then, it applies the given expression to that list to transform and filter it.

        Foreach operators evaluate to zero or more instances of the type they generate.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-23-Foreach.json}
    }
    \item {
        Exists operator: identified by 'EXISTS', the exists operator implements the existential quantification. It takes a number of identifiers that are bound and iterates over them; then, it applies the given boolean expression to each of the instances in the identifiers' domains to determine if it evaluates to true at least once.

        Exists operators always evaluate to a boolean value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-24-Exists.json}
    }
    \item {
        Forall operator: identified by 'FORALL', the forall operator implements the universal quantification. It takes a number of identifiers that are bound and iterates over them; then, it applies the given boolean expression to each of the instances in the identifiers' domains to determine if it evaluates to true for all of those instances.

        Forall operators always evaluate to a boolean value.

        Example:
        \lstinputlisting[language=json]{Examples/5-Appendix/5-2-eFLINT-operators/5-2-25-Forall.json}
    }
\end{itemize}
