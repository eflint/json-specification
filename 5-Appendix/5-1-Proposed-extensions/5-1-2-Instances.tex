\subsubsection{Instances} \label{sec:appendix-extensions-instances}
\textit{Extension identifier: "instances"}\\
\textit{Extension version: "2.0.0"}

The vanilla specification does not offer support to differentiate between various states. That is, every request is processed using the same knowledge base. And while this works suitably well for situations where there is only one client, this might become impractical in multi-client scenario's\footnote{Note that this is only true for situations where clients use stateful requests. However, since every client can decide to do so at their discretion, the risk of anyone changing the state (by accident) is still present.}.

To that end, this extension introduces the concept of \textit{instances}, which allows clients to switch between different knowledge bases to process their requests. The following lifecycle is introduced: first, clients create a new instance, then they can send phrases-requests in the context of these instances, after which they conclude their interaction by destroying the instance.

This extension makes a difference between two types of instances: \textit{manual} instances and \textit{non-manual} instances. The former are created using the lifecycle above, whereas the latter is created by the server. As such, the latter cannot be managed by users.

In order to be compatible with the vanilla specification, server must \textit{always} provide a non-manual \textit{default}-instance. As can be seen below, this is what the server should default to if no specific instance is given.

To support this interaction, three new types of requests are introduced, three new types and two existing requests are extended with some fields. These will be discussed in the next few subsections.

\subsubsubsection{The instance-new request} \label{sec:appendix-extensions-instances-instance-new}
\textit{Identified by: "instance-new"}

This request instructs the server to instantiate a new instance, i.e., knowledge base. Specifically, this new instance should be empty; it should be equivalent to starting a new server without any (stateful) requests to it.

The following fields are required as part of the instance-new request:
\begin{itemize}
    \item {
        \texttt{name}: A JSON String that determines the name of the new instance. This is used to refer to it, and as such must be unique among the already existing instances. Because the default instance always exists, this cannot be: "default".\\
        The server is free to implement additional constraints on the form of the name. A typical constraint is that it may only consist of alphanumeric characters, dashes and underscores.
    }
    \item {
        \texttt{authentication}: An \textit{optional} \textbf{authentication}-type (see section \ref{sec:appendix-extensions-instances-authentication-type}) that allows users to provide some type of authentication to access a particular instance.\\
        Note that the server \textit{is} permitted to impose authentication restrictions on the default instance.\\
        If this field is omitted, it defaults to a none-authentication (see section \ref{sec:appendix-extensions-instances-authentication-none}).
    }
    \item {
        \texttt{timeout}: An \textit{optional} JSON number that specifies the number of seconds of no usage of the instance before the server is free to remove it. A value of 0 means that the client wishes the instance to be available indefinitely.\\
        This value is used to allow the server to take client wishes into account when pruning unused instances. Note, though, that these are indeed client \textit{wishes}; the server is free to ignore them. As such, clients are encouraged to always handle unexpected "unknown instance" errors.\\
        If omitted, this field defaults to a value set by the server. A client can obtain this value by sending a handshake-request (see section \ref{sec:handshake-request}; or the extension in section \ref{sec:appendix-extensions-instances-handshake-extension}).
    }
\end{itemize}

The response to instance-new requests is defined by an instance-new response, which does not add any fields other than the already present \texttt{success}-field. As such, if the server returns that the request was processed successfully, the client may assume their new instance is created and available for use in requests.

The following are example instance-new requests and matching responses:
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-1-Instance-new-1.json}
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-2-Instance-new-2.json}
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-3-Instance-new-3.json}
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-4-Instance-new-4.json}


\subsubsubsection{The instance-del request} \label{sec:appendix-extensions-instances-instance-del}
\textit{Identified by: "instance-del"}

This request instructs the server to destroy an existing instance, removing it from active memory. The client can do this to remove any state from the server and to explicit release the associated instance resource.

For obvious purposes, only instances to which the client has access can be deleted, i.e., instances for which the client can pass the authentication check. Non-manual instances cannot be deleted through this request.

The following fields are part of the instance-del request:
\begin{itemize}
    \item {
        \texttt{name}: A JSON String specifying the name of the instance to remove. This name should be the name of a manual instance that still exists.
    }
    \item {
        \texttt{authentication}: An \textit{optional} \textbf{authentication}-type (see section \ref{sec:appendix-extensions-instances-authentication-type}) that allows users to provide some type of authentication to access a particular instance.\\
        Note that the server \textit{is} permitted to impose authentication restrictions on the default instance.\\
        If this field is omitted, it defaults to a none-authentication (see section \ref{sec:appendix-extensions-instances-authentication-none}).
    }
\end{itemize}

An instance-del request is replied to with an instance-del response, which does not have additional fields over the ones already common to all responses. This means that the client can assume the instance has been successfully removed when it receives a successful response.

The following are examples of instance-del requests and their responses:
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-5-Instance-del-1.json}
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-6-Instance-del-2.json}
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-7-Instance-del-3.json}


\subsubsubsection{The instance-list request} \label{sec:appendix-extensions-instances-instance-list}
\textit{Identified by: "instance-list"}

The instance-list request can be used by clients to discover existing instances and if they are public or private.

To submit an instance-list request, a toplevel request without any non-common fields can be send.

Then, the server responds with an instance-list response with the following fields:
\begin{itemize}
    \item {
        \texttt{instances}: A JSON Array of one or more \textbf{instance}-types (see section \ref{sec:appendix-extensions-instances-instance-type}) that lists the instances currently available in the instance. Note that the list of instances returned may be in any order, not necessarily the order of creation.
    }
\end{itemize}

The following presents an example interaction for an instance-list request:
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-8-Instance-list.json}


\subsubsubsection{instance type} \label{sec:appendix-extensions-instances-instance-type}

In order to communicate which instances are defined at any one time in the instance, this extension contributes the \textbf{instance}-type to represent the information known about an instance.

An \textbf{instance}-type has the following fields:
\begin{itemize}
    \item {
        \texttt{name}: A JSON String representing the name of the instance.
    }
    \item {
        \texttt{is-manual}: A JSON boolean indicating whether this is a manual instance or not. If it is (i.e., a user created this instance), then this field will be true; otherwise (i.e., the instance was created by the server, e.g., the default-instance), then the this field will be false.\\
        This is relevant to know for users because it determines whether they can attempt to delete that instance even if they have access to it.
    }
    \item {
        \texttt{authentication}: An \textit{optional} \textbf{authentication-kind}-type (see section \ref{sec:appendix-extensions-instances-authentication-kind-type}) indicating the authentication variant used for this instance. Possible values are equal to the possible values for the \texttt{kind}-field in the \textbf{authentication}-type (see section \ref{sec:appendix-extensions-instances-authentication-type}).\\
        If omitted, or if this is null, then it defaults to the none-variant (section \ref{sec:appendix-extensions-instances-authentication-kind-none}).
    }
    \item {
        \texttt{timeout}: A JSON number indicating the timeout of the instance. This represents the number of seconds of inactivity before the server is free to delete the instance. A timeout of 0 means that the instance should be kept indefinitely (although the server is still free to delete it earlier if it must).
    }
\end{itemize}

A few examples:
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-9-Instance-type.json}


\subsubsubsection{authentication type} \label{sec:appendix-extensions-instances-authentication-type}

In real-world scenarios, authentication schemes can vary widly, from either no scheme, to user/password authentication, or even to certificate-based authentication. In order to support this variaty, the \textbf{authentication}-type is introduced, which can have multiple variants to express these different kinds of authentications.

All variants of the \textbf{authentication}-type are JSON Objects with at least the following field:
\begin{itemize}
    \item {
        \texttt{kind}: A JSON String that encodes the identifier for this variant.
    }
\end{itemize}

Servers are free to define their own variants of the \textbf{authentication}-type, in order to implement their own authentication schemes. However, the following variant is part of the proposal and \textit{must} be implemented by servers:
\begin{itemize}
    \item { \label{sec:appendix-extensions-instances-authentication-none}
        \textit{none}: The none-variant encodes that no authentication scheme is used. It is identifiable using the identifier: "none".\\
        It defines no additional fields. 
    }
\end{itemize}
Then there is also another variant which is pre-defined but not necessarily implemented:
\begin{itemize}
    \item { \label{sec:appendix-extensions-instances-authentication-password}
        \textit{password}: The password-variant encodes that a simple password is used to access an instance. It is identifiable using the identifier: "password".\\
        It defines the following additional fields:
        \begin{itemize}
            \item {
                \texttt{pass}: A JSON String carrying the password used for authentication. This password is assumed to be plaintext by the server, and so it is recommended to only use this authentication scheme over encrypted connections.
            }
        \end{itemize}
    }
\end{itemize}

The following snippets encode examples of the two pre-implemented \textbf{authentication} variants:
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-10-Authentication-type-1.json}
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-11-Authentication-type-2.json}


\subsubsubsection{authentication-kind type} \label{sec:appendix-extensions-instances-authentication-kind-type}

Matching the different \textbf{authentication}-type variants (see section \ref{sec:appendix-extensions-instances-authentication-type}), there is also the \textbf{authentication-kind} type which provides a description to users so they know which scheme and which settings for that scheme are used.

The \textbf{authentication-kind} type \textbf{must} implement the same variants as the \textbf{authentication}-type, but the variants may have a different body. Like \textbf{authentication}, the kind-type must be a JSON Object with at least:
\begin{itemize}
    \item {
        \texttt{kind}: A JSON String that encodes the identifier for this variant.
    }
\end{itemize}

\label{sec:appendix-extensions-instances-authentication-kind-none}\label{sec:appendix-extensions-instances-authentication-kind-password}Because all servers need to implement "none", the "none"-variant for \textbf{authentication-kind} must also exist. It has no additional fields. Similarly, if servers implement the "password"-variant for \textbf{authentication}, then the "password"-variant for \textbf{authentication-kind} must be implemented too. It also has no additional fields.

Examples:
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-12-Authentication-kind-type-1.json}
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-13-Authentication-kind-type-2.json}
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-14-Authentication-kind-type-3.json}


\subsubsubsection{Extension to the phrases request} \label{sec:appendix-extensions-instances-phrases-extension}

This extension extends the generic phrases request (section \ref{sec:phrases-request}) with additional fields in order to contextualize phrases within instances:
\begin{itemize}
    \item {
        \texttt{instance}: An \textit{optional} JSON String determining which instance to use for resolving this phrases request. This must refer to an existing instance. It omitted, then this defaults to "default" (i.e., use the default instance).
    }
    \item {
        \texttt{instance-authentication}: An \textit{optional} \textbf{authentication}-type (see section \ref{sec:appendix-extensions-instances-authentication-type}) providing the authentication details needed to access the particular instance given in \textit{instance}. If omitted, then it defaults to the "none"-variant (section \ref{sec:appendix-extensions-instances-authentication-none}).
    }
\end{itemize}

For example, the following is an example phrases request with a specific instance:
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-15-Phrases-request-1.json}
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-16-Phrases-request-2.json}


\subsubsubsection{Extension to the handshake response} \label{sec:appendix-extensions-instances-handshake-extension}

As the final contribution of this extension, the response to the handshake-request (section \ref{sec:handshake-request}) is extended with an additional property reported by the server.

In particular, the following field is added to the handshake-response:
\begin{itemize}
    \item {
        \texttt{instance-timeout}: An \textit{optional} JSON number that indicates the default value of the \texttt{timeout}-field in an instance-new request (section \ref{sec:appendix-extensions-instances-instance-new}). If omitted, then the server does not want to share this information and the client can only discover this by observing if the server deletes instances periodically.
    }
\end{itemize}

For example:
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-2-Instances/5-1-2-17-Handshake-request.json}
