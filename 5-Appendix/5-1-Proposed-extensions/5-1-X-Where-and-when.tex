\subsubsection{Where and When} \label{sec:appendix-extensions-where-when}
\textit{Extension identifier: "where\_when"}

In eFLINT, there exists a contentious additional clause for Facts, Events, Acts and Duties: the Where-clause (for which When-clauses are aliases). While not part of the main specification, this variation proposes support for them that can be optionally enabled by clients if desired.

A Where-clause is semantically very similar to a Conditioned by-clause. Like the Conditioned by, it restricts which instances of a type may be derived by providing an expression for evaluating them. However, in contrast to a Condition by, Where-clauses \textit{also} restrict postulation, even preventing the type from being "manually" constructed. Practically, this means that it can be used to dynamically limit the domain of a particular Fact, Event, Act or Duty.

To illustrate, consider the following eFLINT snippet:
\begin{lstlisting}[language=eflint]
// Citizens exist, who can have an age
Fact citizen Identified by String.
Fact age Identified by citizen * int.
// They have a voting-age too if they are allowed to vote
Fact voting-age Identified by age
    // Note we use a Condition by here...
    Conditioned by age.int >= 18
    Holds when age.
Fact voting-age-strict Identified by age
    // ...but a Where here
    Where age.int >= 18
    Holds when age.

// See if Amy has a voting-age if she is 8
+citizen(Amy).
+age(citizen(Amy), 8).
?voting-age(age(citizen(Amy), 8)).          // False
?voting-age-strict(age(citizen(Amy), 8)).   // False

// Posulate Amy has a voting age; then check again
+voting-age(age(citizen(Amy), 8)).
+voting-age-strict(age(citizen(Amy), 8)).
?voting-age(age(citizen(Amy), 8)).          // True
?voting-age-strict(age(citizen(Amy), 8)).   // False
\end{lstlisting}

This shows that a Condition by-clause can be "circumvented" by postulation, but a Where-clause can not.

The reason for this dynamicity is what makes this clause contentions, since it makes eFLINT domains dynamic instead of static. This is still not well understood, and may leave to unpredictable behaviour.

To use the Where-clauses, one can specify the following additional fields for Facts, Events, Acts and Duties:
\begin{enumerate}
    \item {
        \texttt{where}: An \textit{optional} JSON Array of \textbf{expression}-types (see section \ref{sec:expression-type}) that restricts the types that can exist.\\
        \texttt{where}-fields are conjunctive, i.e., for the postulation of a type to happen, all of its \texttt{where}-clauses must be satisfied (evaluate to true). The same holds for derivation, except that also all \texttt{condition-by} clauses must be satisfied.\\
        The expressions in the \texttt{where}-field must be boolean expressions. If this field is omitted, it will default to an empty array, implying that there are no additional conditions for derivation or postulation.
    }
    \item {
         \texttt{when}: Alias for the \texttt{where}-field. Specifically, implementations should treat any \texttt{when}-field exactly as if it was a \texttt{where}-field.
    }
\end{enumerate}
Servers supporting this variation must then take these additional clauses into account as described in this variation.

For example, the following would be a valid request:
\begin{lstlisting}[language=json]
// We send to the server a little scenario, where there are two
// candidates and a derived fact determining the winner
{
    "version": "1.0.0-where_when",
    "kind": "phrases",
    "phrases": {
        // Fact voter
        {
            "kind": "afact",
            "name": "voter"
        },
        // Fact candidate
        {
            "kind": "afact",
            "name": "candidate"
        },
        // Fact has-voted Identified by voter * candidate
        {
            "kind": "cfact",
            "name": "has-voted",
            "identified-by": [ "voter", "candidate" ]
        },
        // Fact is-winner Identifier by candidate
        //   Holds when candidate.
        //   Where Foreach candidate' :
        //     Count(
        //       Foreach voter' : has-voted(voter', candidate)
        //     )
        //     >
        //     Count(
        //       Foreach voter' : has-voted(voter', candidate')
        //     )
        {
            "kind": "cfact",
            "name": "is-winner",
            "holds-when": [ [ "candidate" ] ],
            // The interesting bit
            "where": [{
                "iterator": "FOREACH",
                "binds": [ "candidate'" ],
                "expression": {
                    "operator": "GT",
                    "operands": [
                        // Left-hand side
                        {
                            "iterator": "COUNT",
                            "binds": [ "voter'" ],
                            "expression": [{
                                "identifier": "has-voted",
                                "operands": [
                                    [ "voter'" ],
                                    [ "candidate" ]
                                ]
                            }]
                        },
                        // Right-hand side
                        {
                            "iterator": "COUNT",
                            "binds": [ "voter'" ],
                            "expression": [{
                                "identifier": "has-voted",
                                "operands": [
                                    [ "voter'" ],
                                    [ "candidate'" ]
                                ]
                            }]
                        }
                    ]
                }
            }]
        },
        // +candidate(Cho)
        {
            "kind": "create",
            "operand": {
                "identifier": "candidate",
                "operands": [ "Cho" ]
            }
        },
        // +candidate(Dan)
        {
            "kind": "create",
            "operand": {
                "identifier": "candidate",
                "operands": [ "Dan" ]
            }
        },
        // Now Dan attempts to commit fraud by running:
        //   +is-winner(candidate(Dan)).
        {
            "kind": "create",
            "operand": {
                "identifier": "is-winner",
                "operands": [{
                    "identifier": "candidate",
                    "operands": [ "Dan" ]
                }]
            }
        }
    }
}

// The server sends a response with the current state changes
{
    "success": true,
    "results": [
        {
            "success": true,
            "changes"" [
                // This will contain the 'Fact voter' again,
                // omitted for brevity
            ],
            "violated": false
        },
        {
            "success": true,
            "changes"" [
                // This will contain the 'Fact candidate' again,
                // omitted for brevity
            ],
            "violated": false
        },
        {
            "success": true,
            "changes"" [
                // This will contain the 'Fact has-voted' again,
                // omitted for brevity
            ],
            "violated": false
        },
        {
            "success": true,
            "changes"" [
                // This will contain the 'Fact is-winner' again,
                // omitted for brevity
            ],
            "violated": false
        },
        {
            "success": true,
            "changes"" [
                // This will contain the '+candidate(Cho)' again,
                // omitted for brevity
            ],
            "violated": false
        },
        {
            "success": true,
            "changes"" [
                // This will contain the '+candidate(Dan)' again,
                // omitted for brevity
            ],
            "violated": false
        }
        // ...but note the last creation is not here! This is
        // because the postulation was blocked by the Where.
        // Dan's fraud failed!
    ]
}
\end{lstlisting}
