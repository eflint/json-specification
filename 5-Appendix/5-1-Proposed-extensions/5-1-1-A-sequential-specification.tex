\subsubsection{A sequential specification} \label{sec:appendix-extensions-sequential}
\textit{Extension identifier: "sequential"}\\
\textit{Extension version: "1.0.0"}

eFLINT, the main reasoner for which this specification is designed, is constructed to be a \textit{sequential language}. This term is formally defined in \cite{sequential}, but it can be summarised by that a language is sequential if and only if any combination of valid programs in it is a valid program with the same effects. Or, in the case of eFLINT: sending all the phrases as one program is the same as sending those phrases in separate programs, and vice versa.

Ideally, we want the specification to enforce this same property. However, the introduction of stateless phrases (see the common fields in the \textbf{phrase}-type; section \ref{sec:phrase-type}) breaks this property in the general case. As an example of that, consider the following phrases-request:
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-1-A-sequential-specification/5-1-1-1-Example-request.json}

Now, suppose that we were to divide this single request into two requests, each containing half of the phrases:
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-1-A-sequential-specification/5-1-1-2-Example-request-divided.json}
Since sending the same phrases in one request or in two has a different result, this proves that the sequential property no longer holds for the eFLINT specification.

Fortunately, we can define an extension to the Specification where the client imposes additional restrictions upon itself to make it sequential again. The main idea is to treat stateless phrases as 'extra input' instead of fully-fledged phrases, which means that concatenating or splitting requests only concerns itself with the stateful phrases.

Concretely, we will add the following restrictions that the client must uphold when defining phrases requests (section \ref{sec:phrases-request}):
\begin{enumerate}
    \item {
        Instead of arbitrarily interleaving stateless and stateful phrases, each request must now first contain all of its stateless phrases (if any), and then all of its stateful phrases (if any).
    }
    \item {
        When combining two or more requests, the client must concatenate the lists of stateless phrases and that of stateful phrases separately (i.e., requirement 1 is upheld when combining requests). Optionally, any stateless phrases that have no effect (e.g., duplicate phrases) may be removed from the resulting request.
    }
    \item {
        When dividing a request into two or more sub-requests, the client must duplicate the stateless phrases for all of the new requests and then only split the stateful phrases. Optionally, any stateless phrases that do not have an effect may be removed from either request.
    }
\end{enumerate}

Treating requests this way preserves the sequential property for the Specification. As an example, consider the compliant version of the same request as above:
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-1-A-sequential-specification/5-1-1-3-Example-sequential-request.json}

Next, we can divide this request in two in line with the new restrictions:
\lstinputlisting[language=json]{Examples/5-Appendix/5-1-Proposed-extensions/5-1-1-A-sequential-specification/5-1-1-4-Example-sequential-request-divided.json}

Note that the extra restrictions introduced in this variation of the specification are client-side only; that means that any server supporting the main specification also implicitly supports this variant (although they are encouraged to also explicitly advertise they support it).



% Below, we will give brief explanation of the theoretical background behind these restrictions, but this is not relevant for the variant of the specification itself.


% \subsubsubsection{Theoretical background} \label{sec:appendix-variations-sequential-background}
% Formally, we can define a language $L$ that only accepts programs in the set $P$ of the form $p = ( s_1, s_2, \cdots, s_n )$, where $n$ is the number of statements $s$ in the program. Moreover, we define an operation $R(p)$ with $p \in P$ that represents the result of running program $p$. We can also write $R(p_1 + p_2)$ with $p_1, p_2 \in P$, where we represent the result of running programs $p_1$ and $p_2$ in sequence, in that order.

% Under these circumstances, we can say that a language is sequential if and only if there is an operator $\circ$ such that the following two properties hold:
% \begin{align}
%     p_1 \circ p_2 &\in P \label{eq:sequential-property-1}\\
%     R(p_1 + p_2) &= R(p_1 \circ p_2) \label{eq:sequential-property-2}
% \end{align}
% for any pair $p_1, p_2 \in P$.

% In the normal specification, we use this as a 'naive' formalisation of eFLINT: it is a language that accepts programs of zero or more stateless or stateful phrases. Because, as seen above, the arbitrary interlacing of stateless and stateful phrases means that it is hard to define such an operator $\circ$

% With these definitions in mind, we can formally represent our own definition of a sequential language, which can be found in definition~\ref{def:sequential-language}.
% \begin{definition} \label{def:sequential-language}
%   A language $L$ with a set of valid programs $P$ is said to be sequential if and only if, for every pair of valid programs $p_1, p_2 \in P$, it has an operator $\circ$ such that $p_1 \circ p_2 \in P$. Moreover, if $R(p)$ is the result of program $p \in P$ and $R(p_i + p_j)$ defines the result of running program $p_i$ and $p_j$ in sequence, then the following must also hold true for every $p_1, p_2 \in P$: $R({p_1 + p_2}) = R(p_1 \circ p_2)$
% \end{definition}

% Conceptually, we will change our notion of a \textit{program} (i.e., what a request implements) to achieve this. Instead of a program being a sequence of zero or more \textit{statements} (i.e., phrases) to execute, we extend each statement with a separate collection called \textit{input}. Formally, our notion changes from
% \begin{equation} \label{eq:old-program}
%     p^n = ( s_1, s_2, \cdots, s_n )
% \end{equation}
% to
% \begin{equation} \label{eq:new-program}
%     p^n = ( ( I_1, s_1 ), ( I_2, s_2 ), \cdots, ( I_n, s_n ) )
% \end{equation}
% Here, $p^n$ is a program with $n$ statements, $s_x$ is the $x$'th statement of the program and $I_x$ is the input collection for the $x$'th statement.

% This input can be thought of as extra information that a phrases uses to execute besides the state of the reasoner. To make the specification sequential, we want this collection to contain stateless phrases; however, because 

% Conceptually, we will change our notion of a \textit{program} (i.e., what a request implements) to achieve this. Instead of it being a sequence of stateful or stateless phrases that are interleaved arbitrarily, we define a program as a pair of \textit{input}, which is a sequence of zero or more stateless phrases, and a sequence of zero or more stateful phrases (i.e., we strictly separate the two types of phrases). 

% \begin{align} \label{eq:new-program}
%     p^{n \times m} &= ( I^n, ( s_1, s_2, \cdots, s_m) ) \nonumber \\
%                   &= ( ( i_1, i_2, \cdots, i_n), ( s_1, s_2, \cdots, s_m) )
% \end{align}
% where $n \times m$ is the number of stateless and stateful phrases in the program, respectively; $I^x$ is the input with $x$ stateless phrases; $i_x$ is the $x$'th input phrase of the program and $s_x$ is the $x$'th phrase in the program.

% \begin{definition} \label{def:sequential-join}
%   If a language $L$ has a set of valid programs $P$ where $p \in P$ is of the form $p = ( (I_1, s_1), (I_2, s_2), \cdots, (I_n, s_n) )$ (where $I_x$ is the input for phrase $x$, $s \in S$ is the set of all valid phrases and $n$ is the number of phrases in the program), then we can define an operator $\circ$ such that:
%   \begin{align}
%       p_1 \circ p_2 &= ( (I^1_1, s^1_1), \cdots, (I^1_n, s^1_n) ) \circ ( (I^2_1, s^2_1), \cdots, (I^2_m, s^2_m) ) \nonumber\\
%                     &= ( (I^1_1 \cup I^2_1, s^1_1), \cdots, (I^1_n \cup I^2_n, s^1_n), \circ ( (I^1_1 \cup I^2_1, s^2_1), \cdots, (I^1_n \cup I^2_n, s^2_n) )
%   \end{align}
%   where $p_1, p_2 \in P$
%   %If a language $L$ has a set of valid programs $P$ of the form $P = \{ (I, S), \cdots \}$ (where $I$ is an inpu
%   %A language $L$ with a set of valid programs $P$ is said to be sequential if and only if, for every pair of valid programs $p_1, p_2 \in P$, it has an operator $\circ$ such that $p_1 \circ p_2 \in P$. Moreover, if a program $p$ is a tuple of input $I$ and a sequence of phrases $S$ (i.e., $p = (I, S)$), $R(p)$ is the result of program $p \in P$ and $R(p_i + p_j)$ defines the result of running program $p_i$ and $p_j$ in sequence, then the following must also hold true for every $p_1, p_2 \in P$: $R({p_1 + p_2}) = R(p_1 \circ p_2)$
% \end{definition}

% However, we can define a method of ordering and treating phrases that re-introduce this property; this method will be what this variant of the specification will add. Note that these restrictions are client-only, and do not require additional fields or syntax.

% We start by defining a semantic difference, that will pave the way for the actual method. Instead 

% \begin{definition} \label{def:sequential-language}
%   If a language $L$ has a set of valid programs $P$ of the form $P = \{ (I, S), \cdots \}$ (where $I$ is an inpu
%   A language $L$ with a set of valid programs $P$ is said to be sequential if and only if, for every pair of valid programs $p_1, p_2 \in P$, it has an operator $\circ$ such that $p_1 \circ p_2 \in P$. Moreover, if a program $p$ is a tuple of input $I$ and a sequence of phrases $S$ (i.e., $p = (I, S)$), $R(p)$ is the result of program $p \in P$ and $R(p_i + p_j)$ defines the result of running program $p_i$ and $p_j$ in sequence, then the following must also hold true for every $p_1, p_2 \in P$: $R({p_1 + p_2}) = R(p_1 \circ p_2)$
% \end{definition}

% We can formally encapsulate this new notion of a sequential program 

% Formally, we change our notion of a program $p \in P$ from
% \begin{equation} \label{eq:old_notion}
% p = (s_1, s_2, \cdots, s_n)
% \end{equation}
% , where $p \in P$ and $s_1, s_2, \dots, s_n \in S$, to
% \begin{equation} \label{eq:new_notion}
% p = ( \{ I_1, s_1 \}, \{ I_2, s_2 \}, \cdots, \{ I_n, s_n \} )
% \end{equation}
% where $I_j = (i_j^1, i_j^2, \cdots, i_j^{m_j})$, $m_j$ is the number of input phrases for phrase $j$ and $i_j^k \in S$.



% Armed with this notion, we can now define a new way of ordering phrases requests. Instead of arbitrarily interleaving stateless and stateful phrases, clients are now required to always first list any and all stateless phrases, and then list all stateful phrases. This \textit{nearly} implements the above formal definition of a program, except that we assign the same tuple for all of the phrases (and thus define it only once).

% The only thing left to do is to define how to break up requests. Instead of breaking them up arbitrarily, considering all phrases part of the same program that we may split, we now define \textit{only} the stateful phrases as defining the program; the stateless phrases are, after all, just an input to the normal phrases that are implicitly replicated for each of them.

% Formally, we can thus define an operator $\circ$ that satisfies the following:
% \begin{align}
% R(p_1 \circ p_2) &= R(p_1 + p_2) \nonumber\\
% R(( \{I^1, s_1^1\}, \cdots, \{I^1, s_n^1\} ) + ( \{I^2, s_1^2\}, \cdots, \{I^2, s_n^2\} )) &= R(( \{I^1 + I^2, s_1^2\}, \cdots, \{I^2, s_n^2\} ))  \label{eq:splitting_requests}
% ( \{ (i_1^1, i_1^2, \cdots, i_1^{m^1}), s_1 \}, \cdots, \{ (i_n^1, i_n^2, \cdots, i_n^{m^n}), s_n \} )
% \end{align}
