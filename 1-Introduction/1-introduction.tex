\section{Introduction} \label{sec:introduction}
This document specifies an API-interface to interact with an eFLINT\cite{eflint-1}\cite{eflint-2} server from a (remote) client. Its purpose is to abstract away over specific eFLINT-syntax, and instead provide a standardised way for clients to interact with a reasoner. Moreover, some of its elements are specified more general than in the eFLINT language, allowing for a flexible backend eFLINT implementation or even other reasoners.

This document does not concern itself with the way that the JSON is transported to the server (e.g., HTTP, gRPC, etc); only with how the server should respond given the JSON Schema it received.

In this document, we will first discuss some of the design requirements (section \ref{sec:interaction-requirements}) and conventions (section \ref{sec:conventions}) that will help to understand the specification better. Then, in section \ref{sec:about-versioning}, we will briefly touch upon some guarantees about the specification itself, after which we start with the specification's general overview in section \ref{sec:toplevel-interaction}. In section \ref{sec:"types"} we will discuss nested JSON Schemas, and we finally close with the appendix (section \ref{sec:appendix}) where we list some reasoner-specific properties, such as operators and errors, that are not part of the specification itself but very useful to know when attempting to communicate with a reasoner.


\subsection{Interaction requirements} \label{sec:interaction-requirements}
First and foremost, the specification should be able to fully express the eFLINT language. That means that it will have to be able to express its phrases, which consist of different queries, statements, definitions and expressions. Additionally, it will also have to be able to express any result that an eFLINT reasoner may wish to send back to the user.

Because the eFLINT server is moving to a design where it can be operated both on a stateful as a stateless basis (and something in between), the client and server should have the ability to differ between these paradigms in a finegrained manner.

Additionally, because some transactions are stateless, the client should have the ability to express any number of statements per interaction to give the server the opportunity to handle the request in a stateless way. In other words: every interaction between the client and the server should be able to execute the whole range from a single statement to a whole eFLINT program.

Sometimes, the client will want to switch from operating in a stateful to a stateless way or vice versa. To this end, the client should have the ability to 'download' the current state from the eFLINT server in order to extend on or replace parts of it. This means that multiple types of interaction should be supported; some are statements or queries, but others are requests to download the state or send some other commands to the server.


\subsection{Conventions} \label{sec:conventions}
The remainder of this document follows a couple of conventions to aid in clarity. We provide a list for easy overview and reference:
\begin{itemize}
    \item {
        We will use: 'JSON Specification' or 'eFLINT Specification' to refer to this document both as the specification and interfaces defined therein.
    }
    \item {
        Our JSON terminology is based on the Core Definitions and Terminology document part of the JSON Schema project\footnote{\url{https://json-schema.org/draft/2020-12/json-schema-core.html}}. In case of confusion, you should refer to that document to read a particular term's definition.
    }
    \item {
        We will define our own \textit{"types"}, which are JSON Schemas that are nested within the toplevel Schema(s).
    }
    \item {
        Each of those "types" will have its own identifier, written in \textbf{bold} to indicate it is one of our own types.
    }
    \item {
        JSON field names will be written in \texttt{monospace}.
    }
    \item {
        We will write comments in JSON snippets, but we are aware that this is a sensitive subject and not supported by most JSON interpreters. These are only given here to clarify the snippets; they are not part of the interaction we specify here.
    }
    \item {
        Throughout this document, we will use a consistent running example to show example statements that we will map to the equivalent JSON requests. In the example, we will attempt to model an election, where citizens may be allowed to vote on some set of candidates. This is the same running example as used in (some of) the eFLINT documentation, so we hope that this allows users already familiar with eFLINT to recognise the same constructs.\\
        A full implementation of the running example may be found on the specification's github\footnote{\url{https://gitlab.com/eflint/json-specification/-/tree/main/examples/running-example}}.
    }
\end{itemize}
