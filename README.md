# eFLINT JSON Specification
[eFLINT](https://gitlab.com/eflint) is a normative reasoner with an associated DSL for expressing the normative constructs. However, while this DSL is nice for humans to write, software often prefers to communicate in commonly used dataformats such as [JSON](https://www.json.org) or [YAML](https://yaml.org). To provide this functionality to the eFLINT reasoner, this repository specifies a JSON schema for communicating eFLINT snippets with a reasoner.

The aim of this specification is to formalise the communication between a client and a normative reasoner server. This is done in the context of the [Reasoner JSON Specification](TODO), which provides additional functionality such as policy versioning, 

Currently, the specification is used or supported by the following projects:
- [Olaf Erkemeij's eFLINT server](https://github.com/Olaf-Erkemeij/eflint-server)
- [eFLINT JSON Specification Rust implementation](https://gitlab.com/eflint/json-spec-rs)
- [Brane Policy Reasoner](https://github.com/epi-project/policy-reasoner)


## Structure
This repository contains the source LaTeX files for defining the specification.

Examples are separated-out as pure JSON, found in the [`Examples/`](Examples/)-folder. Note that the examples are not necessarily usable immediately, as they contain some yet-to-be-resolved macros and comments. You can obtain usability-friendly versions by running the [`examplify.py`](./examplify.py) script.

In addition, the [releases](https://gitlab.com/eflint/json-specification/-/releases)-tab contains compiled and published specification documents are published for various versions. Simply select the version of your choice and look under the `Downloads`-section.


## Building the document
The easiest way to build the document is to download this repository and then open it with [Overleaf](https://overleaf.com). Alternatively, any local LaTeX compiler should work, too.


## Running the examples
To run the examples, grab an eFLINT reasoner that supports running the eFLINT or JSON files. One such reasoner is [Olaf Erkemeij's eFLINT server](https://github.com/Olaf-Erkemeij/eflint-server), which you can install using the [README](https://github.com/Olaf-Erkemeij/eflint-server/blob/main/README.md) in that repository.

Alternatively, if you have Docker, the following commands should spawn the server:
```bash
git clone https://github.com/Olaf-Erkemeij/eflint-server && cd eflint-server
docker build --tag eflint-server:latest -f ./Dockerfile .
docker run --name eflint-server -d -p 8080:8080 eflint-server
```

Once a reasoner or a server is running, you can feed it the example JSON files and examine the results. One way to do so is to use the [`curl`](https://curl.se/) command-line tool:
```bash
curl --data "@<PATH_TO_JSON_FILE>" localhost:8080
```


### Converting eFLINT to JSON
The eFLINT server we use also has a parser available that converts the `.eflint` files to JSON. You can use the `eflint-to-json` binary for this:
```bash
eflint-to-json <PATH_TO_EFLINT_FILE>
```
The resulting JSON is then written to stdout. You can save this to a file by piping the output:
```bash
eflint-to-json <PATH_TO_EFLINT_FILE> > <PATH_TO_JSON_FILE>
```

This resulting file can then be compared to the provided JSON file, or can be fed to the eFLINT server by using the curl-command above.


## Contribution
If you want to contribute to this project, feel free to drop a [pull request](https://gitlab.com/eflint/json-specification/-/merge_requests) or [raise an issue](https://gitlab.com/eflint/json-specification/issues). Issues are also great way to discuss specification features or suggestions.


## License
The eFLINT JSON Specification is licensed under TODO.
