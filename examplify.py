#!/usr/bin/env python3
# EXAMPLIFY.py
#   by Lut99
#
# Created:
#   23 Apr 2024, 10:11:09
# Last edited:
#   23 Apr 2024, 17:13:18
# Auto updated?
#   Yes
#
# Description:
#   Script that turns LaTeX source examples into usable JSON files.
#   
#   Output files are written to the same folder as input files, but then with `-clean` appended to their name.
#   
#   Concretely:
#   - Resolves `@\specversion@` to the current specification version;
#   - Removes comments; and
#   - Substitutes the toplevel with a list to allow a file with a request/response pair to be parsed.
#

import argparse
import os
import pathlib
import sys
import typing


##### HELPER FUNCTIONS #####
def supports_color() -> bool:
    """
        Returns True if the running system's terminal supports color, and False
        otherwise.

        From: https://stackoverflow.com/a/22254892
    """
    plat = sys.platform
    supported_platform = plat != 'Pocket PC' and (plat != 'win32' or
                                                  'ANSICON' in os.environ)
    # isatty is not always implemented, #6223.
    is_a_tty = hasattr(sys.stdout, 'isatty') and sys.stdout.isatty()
    return supported_platform and is_a_tty

def info(text: str, file=sys.stdout):
    """
        Logs something in error mode, meaning it will have nice colours.
    """

    # Decide on colors
    colors = supports_color()
    accent = "\033[94m" if colors else ""
    bold = "\033[1m" if colors else ""
    end = "\033[0m" if colors else ""
    print(f"{accent}INFO{end}{bold}: {text}{end}", file=file)

def warn(text: str, file=sys.stdout):
    """
        Logs something in error mode, meaning it will have nice colours.
    """

    # Decide on colors
    colors = supports_color()
    accent = "\033[92m" if colors else ""
    bold = "\033[1m" if colors else ""
    end = "\033[0m" if colors else ""
    print(f"{accent}WARNING{end}{bold}: {text}{end}", file=file)

def error(text: str, file=sys.stderr):
    """
        Logs something in error mode, meaning it will have nice colours.
    """

    # Decide on colors
    colors = supports_color()
    accent = "\033[91m" if colors else ""
    bold = "\033[1m" if colors else ""
    end = "\033[0m" if colors else ""
    print(f"{accent}ERROR{end}{bold}: {text}{end}", file=file)





##### FUNCTIONALITY FUNCTIONS #####
def resolve_version(version: typing.Optional[str], main_tex_path: pathlib.Path) -> str:
    """
        Resolves a potentially unset version to the one from main.tex.

        # Arguments
        - `version`: A version the user gave, or `None`.
        - `main_tex_path`: The path to the `main.tex` file we read the version from if `version` is `None`.

        # Returns
        The given `version` if it is not `None`, or else the one from `main.tex`.

        # Errors
        This function may either:
        - throw an `IOError` if `version` is `None` and we failed to read the given `main_tex_path`; or
        - throw a `RuntimeError` if `version` is `None` and we failed to parse it from `main_tex_path`.
    """

    # Do the easy path
    if version is not None:
        return version

    # Otherwise, open the `main.tex` file and read it line-by-line
    with open(main_tex_path, "r") as h:
        while line := h.readline():
            line = line.strip()
            if len(line) > 26 and line[:26] == "\\newcommand{\\specversion}{" and line[-1] == "}":
                version = line[26:-1]
                info(f"Resolved specification version to: '{version}'")
                return version

    # If we didn't find it, error
    raise RuntimeError(f"Failed to find the line with the specification version in '{main_tex_path}'")

def scan_input(input_path: pathlib.Path, extension: str, postfix: str) -> typing.List[typing.Tuple[pathlib.Path, pathlib.Path]]:
    """
        Scans the given input directory for candidate example files to convert.

        # Arguments
        - `input_path`: The path to the input directory to scan. Can also be a file if that's desired.
        - `extension`: The extension to filter files with, i.e., only files with this extension will be converted.
        - `postfix`: Some postfix that is appended to a converted file's name.

        # Returns
        A list of tuples with first the path of the file to convert, and then the path of the file to convert to.

        # Errors
        This function might throw an IOError if we failed to scan the given directory or any nested ones.
    """

    # Start scanning the input directory
    examples = []
    todo = [input_path]
    while len(todo) > 0:
        dir = todo.pop()
        for file in os.listdir(dir):
            # Turn it into a full path
            file_path = dir.joinpath(file)

            # Check what kind of entry it is
            if file_path.is_file():
                # Only store it if the extension matches
                file_ext = ''.join(file_path.suffixes)
                if file_ext == extension:
                    # Produce an output path
                    output_path = file_path.with_name(file_path.name[:-len(file_ext)] + postfix + file_ext)
                    examples.append((file_path, output_path))
            elif file_path.is_dir():
                # Add it to the list of directories to do
                todo.append(file_path)
            else:
                warn(f"Directory entry '{file}' is neither a file or a directory (skipping)")
    return examples

def convert(input_path: pathlib.Path, output_path: pathlib.Path, version: str, dry_run: bool):
    """
        Converts a given input example to a clean output example equivalent.

        # Arguments
        - `input_path`: The path to read the DIRTY example from.
        - `output_path`: The path to write the cleaned example to. Note that it's OK if `input_path` == `output_path`, because we read the whole input file and then close the descriptor before writing.
        - `version`: The version to substitute in the file, potentially.
        - `dry_run`: If given, does not write anything, but instead just lists what it does.

        # Errors
        This function may raise an `IOError` if it failed to either read from the `input_path` or write to the `output_path`.
    """

    info(f"Converting example file '{input_path}' to '{output_path}'...")

    # Read the input
    with open(input_path, "r") as h:
        input = h.read()

    # Go through it line-by-line
    specversion = "@\\specversion@"
    curly_nesting = 0
    square_nesting = 0
    newfile = ""
    for l, line in enumerate(input.splitlines()):
        # Do a little bit of line parsing
        state = 0
        newline = ""
        for c in line:
            if state == 0:
                # Start: watch for comments and strings
                if c == '/':
                    state = 1
                    continue
                elif c == '"':
                    state = 2
                    newline += '"'
                    continue
                elif c == '{':
                    curly_nesting += 1
                    newline += '{'
                    continue
                elif c == '}':
                    curly_nesting -= 1
                    newline += '}'
                    if curly_nesting == 0 and square_nesting == 0:
                        newline += ','
                    elif curly_nesting < 0:
                        curly_nesting = 0
                    continue
                elif c == '[':
                    square_nesting += 1
                    newline += '['
                    continue
                elif c == ']':
                    square_nesting -= 1
                    newline += ']'
                    if curly_nesting == 0 and square_nesting == 0:
                        newline += ','
                    elif square_nesting < 0:
                        square_nesting = 0
                    continue
                else:
                    newline += c
                    continue
            elif state == 1:
                # Seen a slash: watch for the end of a comment and the special `@\specversion@`
                if c == '/':
                    # Comment! rest of this line is ignored
                    if dry_run: info(f" > Skipping comment on line {l + 1}")
                    break
                elif c == '"':
                    # Wasn't a comment, but might be a string. Flush the implicitly buffered slash.
                    state = 2
                    newline += '/'
                    newline += '"'
                    continue
                elif c == '{':
                    curly_nesting += 1
                    newline += '/'
                    newline += '{'
                    continue
                elif c == '}':
                    curly_nesting -= 1
                    newline += '/'
                    newline += '}'
                    if curly_nesting == 0 and square_nesting == 0:
                        newline += ','
                    elif curly_nesting < 0:
                        curly_nesting = 0
                    continue
                elif c == '[':
                    square_nesting += 1
                    newline += '/'
                    newline += '['
                    continue
                elif c == ']':
                    square_nesting -= 1
                    newline += '/'
                    newline += ']'
                    if curly_nesting == 0 and square_nesting == 0:
                        newline += ','
                    elif square_nesting < 0:
                        square_nesting = 0
                    continue
                else:
                    # Wasn't a comment. Go back to start, and flush the implicitly buffered slash
                    state = 0
                    newline += '/'
                    newline += c
                    continue
            elif state == 2:
                # Seen a quote: enter stringmode
                if c == '\\':
                    # Escape mode
                    state = 3 + len(specversion)
                    continue
                elif c == '"':
                    # End of string mode
                    state = 0
                    newline += '"'
                    continue
                elif c == '@':
                    # Could be start of the specversion!
                    state = 3
                    continue
                else:
                    # Just a string word
                    newline += c
                    continue
            elif state >= 3 and state < 3 + (len(specversion) - 1):
                # Seen a '@'; see if we see the rest of the special keyword
                if c == specversion[1 + state - 3]:
                    # So far so good, continue
                    state += 1
                    if state == 3 + (len(specversion) - 1):
                        # Full word! Substitute for the version, and return to string state
                        if dry_run: info(f" > Substituting '{specversion}' with '{version}'")
                        state = 2
                        newline += version
                    continue
                elif c == '\\':
                    # Was not specversion after all, escape mode
                    state = 3 + len(specversion)
                    newline += specversion[:1 + (state - 3)]
                    continue
                elif c == '"':
                    # Was not specversion after all, but end of string
                    state = 0
                    newline += specversion[:1 + (state - 3)]
                    continue
                else:
                    # Was not specversion after all, but remain in the string
                    state = 2
                    newline += specversion[:1 + (state - 3)]
                    continue
            elif state == 3 + len(specversion):
                if c == specversion[0]:
                    # Start the specversion instead
                    state = 3
                    newline += '\\'
                    continue
                else:
                    # Just write the character with backslash, nothing special else (but don't quite the string if it's a quote!)
                    state = 2
                    newline += '\\'
                    newline += c
                    continue

        # Write the line to the output
        newfile += newline + "\n"

    # Wrap the newline in block quotes to make it a list
    newfile = newfile.replace("\n", "\n    ").rstrip()
    if newfile[-1] == ',':
        newfile = newfile[:-1]
    newfile = "[\n    " + newfile + "\n]\n"

    # Then write it if not dry running
    if not dry_run:
        with open(output_path, "w") as h:
            h.write(newfile)





##### ENTRYPOINT #####
def main(input_path: pathlib.Path, extension: str, postfix: str, version: typing.Optional[str], main_tex_path: pathlib.Path, dry_run: bool) -> int:
    """
        Entrypoint to the script.

        # Arguments
        See `examplify.py --help` for an overview of the input arguments.

        # Returns
        The return code of the script, where `0` is success and anything else is failure.
    """

    # Resolve the version first
    try:
        version = resolve_version(version, main_tex_path)
    except IOError as e:
        error(f"Failed to main.tex file '{input_path}': {e}")
        return e.errno
    except RuntimeError as e:
        error(f"Failed to read version number from main.tex file '{input_path}': {e}")
        return 1

    # Scan the input directory
    try:
        examples = scan_input(input_path, extension, postfix)
    except IOError as e:
        error(f"Failed to read input directory '{input_path}': {e}")
        return e.errno
    info(f"Found {len(examples)} example(s) to convert")

    # Then we start to convert them
    for (input_path, output_path) in examples:
        try:
            convert(input_path, output_path, version, dry_run)
        except IOError as e:
            error(f"Failed to convert '{input_path}' to '{output_path}': {e}")
            return e.errno

    # Run
    return 0



# Actualy entrypoint
if __name__ == "__main__":
    # Define the arguments
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("INPUT", type=str, nargs="?", default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "Examples"), help="The folder to scan for example JSON files.")
    parser.add_argument("-e", "--extension", type=str, default=".json", help="All files with the given extension will be processed by the script, and have a clean counterparty generated.")
    parser.add_argument("-p", "--postfix", type=str, default=".clean", help="The postfix to append to the name of processed files. BE AWARE, if you make this empty, you will overwrite the original example files.")
    parser.add_argument("-v", "--version", type=str, help="If given, then does not parse the current spec version from the `main.tex` file, but instead uses the given one.")
    parser.add_argument("-m", "--main-tex", type=str, default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "main.tex"), help="The path to the `main.tex` file to parse the version number from. Ignored if '--version' is given.")
    parser.add_argument("-d", "--dry-run", action="store_true", help="If given, does not actually generate files, but merely shows which would be generated.")

    # Parse the arguments
    args = parser.parse_args()

    # Run main
    exit(main(pathlib.Path(args.INPUT), args.extension, args.postfix, args.version, pathlib.Path(args.main_tex), args.dry_run))
